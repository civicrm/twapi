import { Router, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isEditMode = false;
  navigationSubscription;

  constructor(private _router: Router) { 
    this.injectStyleClassToBodyTag();

    this.navigationSubscription = this._router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.initialiseInvites();
      }
    });

  }

  ngOnInit() {
    
  }

  openEditMode(){
    this.isEditMode = true;
  }

  closeEditMode() {
    this.isEditMode = false;
  }

  injectStyleClassToBodyTag() {
    if (localStorage.getItem("inputs")){
    
      if (!JSON.parse(localStorage.getItem("inputs"))["styleLight"]) {
        document.getElementsByTagName("body")[0].className = "dark";
      } else {
        document.getElementsByTagName("body")[0].className = "light";
      }
    }
    else {
      document.getElementsByTagName("body")[0].className = "light";
    }
  }

  initialiseInvites() {
    this.injectStyleClassToBodyTag();
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {  
       this.navigationSubscription.unsubscribe();
    }
  }
}
