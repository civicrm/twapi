import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class TwitterService {

  constructor(private http: HttpClient) { }
  



  getTweets(screenName) {
    const localData = JSON.parse(localStorage.getItem("inputs"));
 
    let count = "30";
    if (localData.numberOfTweets > 0 && localData.numberOfTweets < 100) {
      count = localData.numberOfTweets;
    }

    let since = "";
    if (localData.fromDate) {
      since = moment(localData.fromDate).format("YYYY-MM-DD");
    }

    let until = "";
    if (localData.fromDate) {
      until = moment(localData.toDate).format("YYYY-MM-DD");
    }
    
    return this.http.get(environment.ApiURL + "statuses/user_timeline.json", { params: {
      count: count,
      screen_name: screenName,
      until: until,
      since: since,
      include_entities: "true",
      tweet_mode: "extended"
    }});
  }
}

