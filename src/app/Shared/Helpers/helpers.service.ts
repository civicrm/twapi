import { Injectable } from '@angular/core';
import { linkify } from 'linkifyjs';
import * as linkifyjs from 'linkifyjs/html';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})

export class HelpersService {

  
  constructor() { }

  Urlify(Text){
    return linkifyjs(Text, {
      defaultProtocol: 'https'
    });
  }

  getFormattedDate(date, format){
    return moment(date).format(format);
  }

  getTimeFromNow(date){
    return moment(date).fromNow();
  }

}
