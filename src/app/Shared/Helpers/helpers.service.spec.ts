import { TestBed } from '@angular/core/testing';

import { HelpersService } from './helpers.service';

describe('HelpersService', () => {
  it('Urlify Function Should Convert any URL in Text to href inside a Tag', () => {
    const service: HelpersService = TestBed.get(HelpersService);
    let textContainsURL = "Check this Link https://google.com ";
    let textWithATag =  service.Urlify(textContainsURL);
    expect(textWithATag).toContain("</a>");
    expect(textWithATag).toContain("href=");
  });

  it('getFormattedDate Should Formatted any String Date', () => {
    const service: HelpersService = TestBed.get(HelpersService);
    let UnformattedStringDate = "Mon Nov 19 2018 17:06:39 GMT+0300";
    let formattedStringDate = service.getFormattedDate(UnformattedStringDate, "YYYY-MM-DD");
    
    expect(formattedStringDate).toBe("2018-11-19");
  });


});
