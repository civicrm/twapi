import { HomePageComponent } from './Pages/home-page/home-page.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { HeaderComponent } from './Shared/Partial/header/header.component';
import { FooterComponent } from './Shared/Partial/footer/footer.component';
import { TweetComponent } from './Components/tweet/tweet.component';
import { HttpClientModule } from '@angular/common/http';
import { TweetsPreLoaderComponent } from './Components/tweets-pre-loader/tweets-pre-loader.component';
import { SortColumnsOverlayComponent } from './Components/sort-columns-overlay/sort-columns-overlay.component';
import { TwitteSettingModalComponent } from './Components/twitte-setting-modal/twitte-setting-modal.component';


import { AccordionModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { RadioButtonModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { SpinnerModule } from 'primeng/primeng';
import { InputSwitchModule } from 'primeng/primeng';




import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

const appRoutes: Routes = [
  { path: '', component: HomePageComponent, runGuardsAndResolvers: "always"},
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    FooterComponent,
    TweetComponent,
    TweetsPreLoaderComponent,
    SortColumnsOverlayComponent,
    TwitteSettingModalComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AccordionModule,
    PanelModule,
    ButtonModule,
    CalendarModule,
    InputSwitchModule,
    SpinnerModule,
    RadioButtonModule,
    BrowserAnimationsModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, {onSameUrlNavigation: "reload", preloadingStrategy: PreloadAllModules}),
  ],
  providers: [],
  bootstrap: [AppComponent]

})
export class AppModule { }
