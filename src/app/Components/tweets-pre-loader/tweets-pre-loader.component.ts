import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tweets-pre-loader',
  templateUrl: './tweets-pre-loader.component.html',
  styleUrls: ['./tweets-pre-loader.component.scss']
})
export class TweetsPreLoaderComponent implements OnInit {
  randomHasImageBoolean;
  constructor() { 
    this.randomHasImageBoolean = Math.random() >= 0.5;
  }

  ngOnInit() {

    
  }

}
