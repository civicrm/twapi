import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sort-columns-overlay',
  templateUrl: './sort-columns-overlay.component.html',
  styleUrls: ['./sort-columns-overlay.component.scss']
})
export class SortColumnsOverlayComponent implements OnInit {

  @Output() closeSortMode: EventEmitter<any> = new EventEmitter();
  @Output() storeSort: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  fireCloseSortMode() {
    this.closeSortMode.emit(true);
  }
  
  fireStoreSort(index) {
    this.storeSort.emit(index);
    this.closeSortMode.emit(true);
  }
}
