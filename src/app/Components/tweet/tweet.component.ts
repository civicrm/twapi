import { HelpersService } from './../../Shared/Helpers/helpers.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.scss']
})
export class TweetComponent implements OnInit {

  @Input() item;
  @Input() user;
  constructor(
    private _helpers : HelpersService
  ) { }

  ngOnInit() {
  }

  itemHasImage() {
    return (
      this.item.extended_entities &&
      this.item.extended_entities.media &&
      this.item.extended_entities.media[0]
    );
  }


  formatTweetText(TweetText) {
    if (TweetText.includes("http://") || TweetText.includes("https://")) {
      TweetText = this._helpers.Urlify(TweetText); 
    }
    return TweetText;
  }

  getDate() {
    return this._helpers.getTimeFromNow(this.item.created_at);
  }

  getTweetLink() {
    return "https://twitter.com/" + this.user + "/status/" + this.item.id_str;
  }
}
