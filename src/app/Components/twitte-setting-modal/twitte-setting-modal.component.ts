import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-twitte-setting-modal',
  templateUrl: './twitte-setting-modal.component.html',
  styleUrls: ['./twitte-setting-modal.component.scss']
})
export class TwitteSettingModalComponent implements OnInit {
  @Output() onClose: EventEmitter<any> = new EventEmitter();
  fromDate: Date;
  toDate: Date;
  numberOfTweets: Number = 30;
  styleLight: Boolean = true;

  constructor(private _router : Router) { 
    this.fillInputsFromLocalStorage();
  }

  ngOnInit() {

  }

  fillInputsFromLocalStorage() {
    if (localStorage.getItem('inputs')) {
      this.loadSettings();
    } else {
      this.saveSettings();
    }
  }

  saveSettings() {
   
    localStorage.setItem('inputs', JSON.stringify({
      fromDate: this.fromDate,
      toDate: this.toDate,
      numberOfTweets: this.numberOfTweets,
      styleLight: this.styleLight,
    }));

    this._router.navigate([], { queryParams: {} });

    
  }

  loadSettings() {
    let localData = JSON.parse(localStorage.getItem('inputs'));

   
    this.fromDate = new Date(localData.fromDate);
    this.toDate = new Date(localData.toDate);
    this.numberOfTweets = localData.numberOfTweets;
    this.styleLight = localData.styleLight;
  }

  close () {
    this.onClose.emit(true);
  }
}
