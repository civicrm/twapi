import { Component, OnInit } from '@angular/core';
import { TwitterService } from 'src/app/Shared/Services/twitter.service';
import {CalendarModule} from 'primeng/calendar';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']

})
export class HomePageComponent implements OnInit {
  makeSchool_Tweets;
  newsYCombinator_Tweets;
  yCombinator_Tweets;

  sorts = [ 'newsycombinator', 'ycombinator','MakeSchool']
  selectedColInSortMode = "";
  isSortMode = false;


  numberOfPreloaders = 4;
  numberOfPreloaders_Array;


  navigationSubscription;


  constructor(
    private _twitter: TwitterService,
    private _router: Router
  ) { 
    this.numberOfPreloaders_Array = new Array(this.numberOfPreloaders);

    //Overwrite Default Sorts
    if (localStorage.getItem("columns_sorts")) {
      this.sorts = JSON.parse(localStorage.getItem("columns_sorts"));
    } else {
      localStorage.setItem("columns_sorts", JSON.stringify(this.sorts));
    }


    this.navigationSubscription = this._router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        this.initialiseInvites();
      }
    });


  }

  ngOnInit() {
  
    this.renderColumns();
  }

  makeschool_observable;
  ycombinator_observable;
  newsycombinator_observable;

  renderColumns(){
    this.makeschool_observable = this._twitter.getTweets("makeschool");
    this.makeschool_observable.subscribe(res => {
      this.makeSchool_Tweets = res;
      console.log(res);
    });

    this.ycombinator_observable = this._twitter.getTweets("ycombinator");
    this.ycombinator_observable.subscribe(res => {
      this.yCombinator_Tweets = res;
     });

    this.newsycombinator_observable = this._twitter.getTweets("newsycombinator");
    this.newsycombinator_observable.subscribe(res => {
      this.newsYCombinator_Tweets = res;
     });
  }

  initialiseInvites() {
    this.renderColumns();
  }
  
  openSortMode(clickedCol) {
    this.selectedColInSortMode = clickedCol;
    this.isSortMode = true;
  }

  closeSortMode() {
    this.isSortMode = false;
  }

  storeNewSort(selectedColIndex) {
    let selectedOldColIndex;
    let replacedColumnName;
    this.sorts.map( (val, i) => {
      if (val == this.selectedColInSortMode) {
        selectedOldColIndex = i;
      }
      if (i == selectedColIndex) {
        replacedColumnName = val;
      }
    });


    this.sorts[selectedOldColIndex] = replacedColumnName;
    this.sorts[selectedColIndex] = this.selectedColInSortMode;
    
    localStorage.setItem("columns_sorts", JSON.stringify(this.sorts))
  }

  getTemplateName(i) {
    return this.sorts[i];
  }


  ngOnDestroy() {
    if (this.navigationSubscription) {  
       this.navigationSubscription.unsubscribe();
    }
    if (this.ycombinator_observable) {
      this.ycombinator_observable.unsubscribe();
    }
    if (this.makeschool_observable) {
      this.makeschool_observable.unsubscribe();
    }
    if (this.newsycombinator_observable) {
      this.newsycombinator_observable.unsubscribe();
    }
  }

}
