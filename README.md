# How To Run

first run the twitter proxy on server by running 

`node server/server.js`

after that in another cli run 
`ng serve`


## Info

- tweets Masonry like 
- Any link inside the tweets converted to click able a tag (using urlify)
- show tweet image if it's contain one (only first image well be visible)
- There is nice looking preloaders before tweet loaded (try it in slow internet speed simulator in google chrome )
  * Preloaders used SCSS Mixin Feature 

- Order Columns in Very Simple Way.. 
	But i Haven't Make it as Drag Drop because it will take more time (which i dosen't have because i work as employee for the meantime until 2018/11/30)
	
- Date Picker for Tweets:
	Please Note, there is no date range allowed in given api, so i had create only the UI for it.
	
- dark/light theme 

- date formated to look like (2 hours ago) using moment.js
- visit tweet source by clicking on button
- responsive 
- just small 2 unit test (i haven't did any integration test)
